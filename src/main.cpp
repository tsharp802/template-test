
#include "ImplCommands.h"
#include "PlatformPins.h"
#include "StrataCoreConfiguration.h"

#include <EventDispatcher.h>
#include <PeriodicCommandDispatcher.h>
#include <SerialLink.h>
#include <generated/AppBuildVersionInfo.h>

using namespace core;

int main()
{
    dispatcher::ISerialLink* serialLink(
        new dispatcher::SerialLink(StrataCoreConfiguration::UART0_TX, 
            StrataCoreConfiguration::UART0_RX, StrataCoreConfiguration::UART_BAUDRATE));

    dispatcher::IEventDispatcher* eventQueue(new dispatcher::EventDispatcher());

    dispatcher::IPeriodicCommandDispatcher* dispatcher(
        new dispatcher::PeriodicCommandDispatcher(serialLink, eventQueue));

    ImplCommands commands(dispatcher);

    commands.setBoardName(StrataCoreConfiguration::BOARD_NAME);
    commands.setPlatformId(StrataCoreConfiguration::PLATFORM_ID);
    commands.setModePin(StrataCoreConfiguration::MODE_INT_PIN);
    commands.setBuildVersionInfo(g_application_version);

    /*!
     *  This is a blocking call and never returns until dispatcher->terminate() is called
     *  from a command.
     */
    dispatcher->run();

    delete dispatcher;
    delete eventQueue;
    delete serialLink;
}
