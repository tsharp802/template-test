//
// Created by Prasanth Vivek 08/05/2019
//

#include "PlatformPins.h"

PlatformPins* PlatformPins::pins = nullptr;

PlatformPins::PlatformPins()
{
    link_led = std::unique_ptr<DigitalOut>(new DigitalOut(LINK_LED_PIN));
} 

PlatformPins::~PlatformPins() {}
