
#include "ImplCommands.h"

using namespace ArduinoJson;

using namespace core;

/*!
 * Commands registration
 */

ImplCommands::ImplCommands(dispatcher::IPeriodicCommandDispatcher *dispatcher)
    : commands::CoreCommands(dispatcher), dispatcher_(dispatcher)
{

    // Peripheral pin initialization
    pins = PlatformPins::getInstance();

    /*! Example for setting up handler for periodic notification to host every 500ms
     * std::bind used here to call member function from this ImplCommands object instance
     * addPeriodicHandler function prototype:
     * bool PeriodicCommandDispatcher::addPeriodicHandler(const std::string& cmd,
     *                                                    PeriodicHandlerFunction handler,
     *                                                    unsigned int interval, int run_count,
     *                                                    bool initially_started)
     * Parameter description:
     *      cmd -> Used to link JSON command string from host to actual command handler.
     *      handler -> Command handler to run.
     *                 Here PeriodicHandlerFunction = std::function<bool()> = std::bind((ImplCommands::*)(), ImplCommands*).
     *      interval -> Time interval between periodic calls to the handler in milliseconds.
     *      run_count -> Number of times command should be scheduled. Set to zero to run once, -1 to run forever.
     *      initially_started -> If true, command will be scheduled to run as soon as periodic handlers are initially enabled.
     *                           If false, command must be started manually later (see command below).
     * 
     * Commands can be started or stoppped using dispatcher_->startPeriodicCommandHandler(<cmd>)
     * and dispatcher_->stopPeriodicCommandHandler(<cmd>), respectively.
     * 
     * Use dispatcher_->updatePeriodicHandler(<cmd>,<interval>, <run_count>)
     * to update the interval and run_count of a periodic handler that is already scheduled to run
     */
    dispatcher_->addPeriodicHandler(
        "example_periodic_command",
        std::bind(&ImplCommands::examplePeriodicCommand, this), 500, -1, true);

    /*! Example for setting up handler for one-shot "example_command" when it's received from host.
     * std::bind used here to bind member function of this ImplCommands object instance
     * to future JSON payload object parameter from host and wrap into callback function for dispatcher.
     * addCommandHandler function prototype:
     * bool PeriodicCommandDispatcher::addCommandHandler(const std::string& command, 
     *                                                   ICommandDispatcher::CommandHandlerFunction handler)
     * Parameter description:
     *      command -> Used to link JSON command string from host to actual command handler.
     *      handler -> Command handler to run.
     *                 Here ICommandDispatcher::CommandHandlerFunction = std::function<bool(const ArduinoJson::JsonObject&)> 
     *                                                                 = std::bind((ImplCommands::*)(), ImplCommands*, std::placeholders::_1)
     */
    dispatcher_->addCommandHandler(
        "example_command",
        std::bind(&ImplCommands::exampleCommand, this, std::placeholders::_1));

    // Blinks the link LED every 250ms when periodic handlers are enabled
    // This functionality is optional, and this handler can be removed if desired
    dispatcher_->addPeriodicHandler(
        "blink_link_led", std::bind(&ImplCommands::blinkLinkLed, this), 250, -1, true);
}

//////////////////////// PERIODIC CALL ///////////////////////////
/*!
 *  examplePeriodicCommand : Periodic API call. This function is called periodically on the dispatcher event queue
 *                           and thus does not need to get explicitly called from the host. The dispatcher will 
 *                           start running this command once "request_platform_id" command is received from the host
 *  Function: Sends notification to host with example payloads
 *  Emits the following JSON:
{
    "notification": {
        "value": "example_periodic_reponse",
        "payload": {
            "bool_array": [
                true,
                true,
                true
            ],
            "bool_array_rval": [
                true,
                false,
                true
            ],
            "bool_vector": [
                false,
                false,
                false
            ],
            "float_array_4dec": [
                1.1111,
                2.2222,
                3.3333
            ],
            "float_array_rval_5dec": [
                7.77778,
                8.88889,
                10
            ],
            "float_vector_6dec": [
                4.444444,
                5.555555,
                6.666666
            ],
            "int_array": [
                1,
                2,
                3
            ],
            "int_array_rval": [
                11,
                12,
                13,
                14,
                15
            ],
            "int_vector": [
                4,
                5,
                6
            ],
            "single_bool": true,
            "single_bool_rval": true,
            "single_float_1dec": 1.1,
            "single_float_2dec": 1.11,
            "single_float_rval_3dec": 2.222,
            "single_int": 1,
            "single_int_rval": 2,
            "single_string": "str_s",
            "string_array": [
                "str_a_1",
                "str_a_2",
                "str_a_3"
            ],
            "string_array_rval": [
                "str_a_rval1",
                "str_a_rval2",
                "str_a_rval3"
            ],
            "string_literal": "str_lit",
            "string_vector": [
                "str_v_1",
                "str_v_2",
                "str_v_3"
            ]
        }
    }
}
 */
bool ImplCommands::examplePeriodicCommand()
{

    /*!   Example Notification object usage
     * 
     *    Add payload key-value pairs to a notification with the add_payload() member function
     *    Single-element payload values are converted to single ArduinoJson::JsonObject, 
     *    arrays/vectors are converted to ArduinoJson::JsonArray.
     *    For single data element, lvalue reference or rvalue are accepted.
     *    For array, lvalue reference or intializer list are accepted.
     *    For vector, only lvalue reference accepted.
     *    WARNING!: Do not pass reference to empty array (e.g. array arr declared as int arr[] = {}) or 
     *                initializer list ({}) to add_payload. Use Notification::empty_array() instead.
     *                Example usage for empty_array() is included below. Empty vectors can be passed as a parameter.
     *   Number of decimal points to use for floats must be specified
     *   For arrays/vectors of floats, number of decimal places specified applies to all array/vector elements.
     *   Examples of the different methods for adding payloads are shown below
     */

    //Single integer, integer array, integer vector example data
    //Any signed/unsigned integral types (e.g. unsigned int, char, uint32_t) can be used as well
    int example_i = 1;
    int example_i_arr[] = {1, 2, 3};
    std::vector<int> example_i_vec = {4, 5, 6};

    //Single float, float array, float vector example data
    //Double can be used as well
    float example_f = 1.111111f;
    float example_f_arr[] = {1.111111f, 2.222222f, 3.333333f};
    std::vector<float> example_f_vec = {4.444444f, 5.555555f, 6.666666f};

    //Single string, string array, string vector example data
    //Const char*, const char[] (string literal) can be used as well
    std::string example_s = "str_s";
    std::string example_s_arr[] = {"str_a_1", "str_a_2", "str_a_3"};
    std::vector<std::string> example_s_vec = {"str_v_1", "str_v_2", "str_v_3"};

    //Single bool, bool array, bool vector example data
    bool example_b = true;
    bool example_b_arr[] = {true, true, true};
    std::vector<bool> example_b_vec = {false, false, false};

    //Create Notification object, add payloads, and emit through dispatcher
    Notification notification("example_periodic_reponse");
    notification.add_payload("single_int", example_i);
    notification.add_payload("single_int_rval", 2); //Int rvalue parameter
    notification.add_payload("int_array", example_i_arr);
    notification.add_payload("int_array_rval", {11, 12, 13, 14, 15}); //Int initializer list as parameter
    notification.add_payload("int_vector", example_i_vec);
    notification.add_payload("single_float_1dec", example_f, 1);                             //Single float rounded to 1 decimal place
    notification.add_payload("single_float_rval_3dec", 2.222222f, 2);                        //Float rvalue parameter rounded to 2 decimal places
    notification.add_payload("float_array_4dec", example_f_arr, 3);                          //Float array with all values rounded to 3 decimal places
    notification.add_payload("float_array_rval_5dec", {7.777777f, 8.888888f, 9.999999f}, 4); //Float initializer list as parameter, all values rounded to 4 decimal places
    notification.add_payload("float_vector_6dec", example_f_vec, 5);                         //Float vector with all values rounded to 5 decimal places
    notification.add_payload("single_string", example_s);
    notification.add_payload("string_literal", "str_lit"); //String literal parameter
    notification.add_payload("string_array", example_s_arr);
    notification.add_payload("string_array_rval", {"str_a_rval1", "str_a_rval2", "str_a_rval3"}); //String literal initializer list as parameter
    notification.add_payload("string_vector", example_s_vec);
    notification.add_payload("single_bool", example_b);
    notification.add_payload("single_bool_rval", true);
    notification.add_payload("bool_array", example_b_arr);
    notification.add_payload("bool_array_rval", {true, false, true}); //Bool initializer list as parameter
    notification.add_payload("bool_vector", example_b_vec);
    notification.send(dispatcher_);

    return true;
}

//////////////////////// ON DEMAND CALL ///////////////////////////
/*!
 *  exampleCommand: This is an on-demand api call
 *  Function: Takes payload from host and reponds with notification containing value sent in payload.
 *            Also demonstrates overwriting existing payloads and sending multiple notifications in a function.
 *  Example host <--> platform communication steps below:
 *  1.) JSON command sent from host to platform: {"cmd":"example_command","payload":{"cmd_data": 100}}
 *  2.) Command dispatcher parses incoming host JSON command and calls exampleCommand on dispatcher event queue 
 *      since the handler was registered with the dispatcher in ImplCommands constructor
 *  3.) exampleCommand runs and emits the following JSON in response:
 *  {
 *   "notification" : {
 *      "value" : "example_command_response",
 *      "payload" : {
 *          "cmd_data" : 100
 *      }
 *    }
 * }
 * {
 *   "notification" : {
 *      "value" : "example_command_response",
 *      "payload" : {
 *          "cmd_data" : 101
 *      }
 *    }
 * }
 * {
 *   "notification" : {
 *      "value" : "example_command_response2",
 *      "payload" : {
 *          "cmd_data2" : 102
 *      }
 *    }
 * }
 */
bool ImplCommands::exampleCommand(const ArduinoJson::JsonObject &payload)
{
    //Extract command data from payload parameter
    //ArduinoJson takes care of casting, but to be explicit, alternate method for assignment below is:
    //int cmd_data = payload["payload"]["cmd_data"].as<int>();
    //See https://arduinojson.org/v5/api/jsonvariant/as/ for supported types
    int cmd_data = payload["payload"]["cmd_data"];

    //Create Notification object, add payload, and emit through dispatcher
    Notification notification("example_command_response");
    notification.add_payload("cmd_data", cmd_data);
    notification.send(dispatcher_);

    //add_payload() function can also be used to overwrite existing
    //payload data by referring to the same key name
    notification.add_payload("cmd_data", cmd_data + 1);
    notification.send(dispatcher_);

    //To send a notification with a different name in the same function,
    //the same Notification object can be used. To do so, use the clear() function to
    //clear all existing payload data and set_value() to change the notification name
    notification.clear();
    notification.set_value("example_command_response2");
    notification.add_payload("cmd_data2", cmd_data + 2);
    notification.send(dispatcher_);

    //If additional instructions are carried out and the Notification payload data is no longer needed,
    //it is recommended to call the clear() function before continuing in order to  to free up memory. Here, the call
    //is unnecessary as the payloads will automatically be deleted when the Notification object falls out of scope
    return true;
}

// Blinks Link LED in the Strata module on the platform when dispatcher periodic commands are enabled
// This function is optional and can be removed if desired
bool ImplCommands::blinkLinkLed()
{
    *pins->link_led = !*pins->link_led;
    return true;
}
