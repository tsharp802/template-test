/*
 *
 * Platform Strata core-specific definitions
 *
 */
#ifndef _STRATACORECONFIGURATION_H_
#define _STRATACORECONFIGURATION_H_

#include <mbed.h>

// Declare some useful functions
extern "C"{
void platform_enable_uart_irq();
void platform_disable_uart_irq();
void platformDetectConnectState();
}

// container class for all platform pins that 
// will be used in the strata core
struct StrataCoreConfiguration {
        // Interrupt pin to detect if host is connected through USB
        // If mode pin is not implemented on hardware, simply define it as NC.
        static constexpr PinName MODE_INT_PIN = PE15;

        // UART definitions; UART0 is used by platformInterface to communicate to Strata
        static constexpr PinName UART0_TX = PB9;     //  TX
        static constexpr PinName UART0_RX = PB10;    //  RX
        static constexpr int UART_BAUDRATE = 115200;  //  This baud must not change. Strata is fixed

        // If EEPROM is not used to store the "class id" platform ID 
        static constexpr char* BOARD_NAME = "WaterHeater Template";
        static constexpr char* PLATFORM_ID = "Template";
};
#endif // _STRATACORECONFIGURATION_H_