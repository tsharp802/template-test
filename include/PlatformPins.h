/*
 *
 * Platform Applictaion-specific definitions
 *
 */
#ifndef _PLATFORMPINS_H_
#define _PLATFORMPINS_H_

#include <mbed.h>
#include <memory>
// container class for all platform pins that 
// will be used in this application
class PlatformPins {
    private:
        static PlatformPins *pins;
        PlatformPins();

    public: 
        ~PlatformPins();
        static PlatformPins* getInstance() {
            if (pins == nullptr) {
                pins = new PlatformPins();
            }
            return pins;
        }

        // Peripheral Pin declaration
        
        std::unique_ptr<DigitalOut> link_led;

        // Peripheral Pin names and properties definition
    
        // link LED directly on waterheater, need function to enable this when Strata is connected
        const PinName LINK_LED_PIN = PF5;
};

#endif // _PLATFORMPINS_H_
