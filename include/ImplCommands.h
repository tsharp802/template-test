
#ifndef IMPL_COMMANDS_H
#define IMPL_COMMANDS_H

#include "PlatformPins.h"

#include <Notification.h>
#include <CoreCommands.h>
#include <IPeriodicCommandDispatcher.h>
#include <ArduinoJson.h>

namespace core
{
namespace dispatcher
{
class IPeriodicCommandDispatcher;
}
}

class ImplCommands : public core::commands::CoreCommands
{
public:
    ImplCommands(core::dispatcher::IPeriodicCommandDispatcher* dispatcher);

    /*!
    *  Example commands go here
    */
    bool exampleCommand(const ArduinoJson::JsonObject &payload);

    bool examplePeriodicCommand();

    bool blinkLinkLed();


private:
    /*!
    *   Platform dispatcher pointer to emit and add command handlers
    */
    core::dispatcher::IPeriodicCommandDispatcher* dispatcher_;
    // platform pins
    PlatformPins *pins;

};
#endif //IMPL_COMMANDS_H
